#Building the Universe Viewer on Windows

Welcome to the instructions on how to compile and build the Universe Viewer.

To build the viewer you must follow the directions provided exactly.  The 
github repository is for development only.  The stable version we encourage most
people to pull the code and build from is on our HG repository on BitBucket.  
Bugs should be reported on our github issue tracker.

#Install Build Software

CMake: https://cmake.org/files/v3.1/cmake-3.1.0-win32-x86.exe

Python: https://www.python.org/ftp/python/2.7.11/python-2.7.11.msi

TortoiseHG: http://tortoisehg.bitbucket.org/download/index.html

TortoiseGit: https://tortoisegit.org/download/

GitSCM: https://git-for-windows.github.io/

Cygwin: http://www.cygwin.com/
packages to be installed in cygwin are
- archive/p7zip
- archive/unzip
- Net/curl

NOTE: The 3 below do not appear to be necessary for our viewer however
Linden Labs uses them and says they are so we will install them for cygwin
just to be safe.
- devel/bison
- devel/flex
- devel/patch

NOTE: The following are optional but highly recommended that you install these
especially if you plan to distribute your viewer.

NSIS: https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/unsis/nsis-2.46.5-Unicode-setup.exe

Notepad++: https://notepad-plus-plus.org/repository/6.x/6.9/npp.6.9.Installer.exe

Restart your PC before moving to installing the next couple of items

After restarting pc:

Install in Windows Command Prompt

Bootstrap pip
    Download (Save As) get-pip.py from git-pip.py: https://bootstrap.pypa.io/get-pip.py and copy to a temp folder
    Open Windows Command Prompt
    Switch to that temp folder and execute it python get-pip.py
    Pip will be installed

ez_setup.py
    Download (Save As) ez_setup.py from ez_setup.py: https://bootstrap.pypa.io/ez_setup.py and copy to a temp folder
    Remain in Windows Command Prompt
    Switch to that temp folder and execute it python ez_setup.py
    easy_install will be installed

Autobuild
    Remain in Windows Command Prompt
    Change to the Python Scripts folder that was just created
    Typically cd \Python27\Scripts
    Run pip install hg+http://bitbucket.org/secondgalaxydevelopmentteam/autobuild-universe
    Autobuild will be installed. Earlier versions of autobuild could be made to work by just putting the source files into your path correctly; this is no longer true - autobuild must be installed as described here


#Test build

To do a test build do as follows in cygwin console:

cd /cygdrive/c/
mkdir work
cd work

check out a copy of the mercural repository for the Universe Viewer as follows

hg clone https://bitbucket.org/secondgalaxydevelopmentteam/universe-viewer

Once this step is completed in cygwin console do the following:

cd viewer-release
autobuild configure -c configuration

where configuration is either "RelWithDebInfoOS" or "ReleaseOS"

When that completes, you can either build within Visual Studio or from the command line


#Command Line Builds

Initialize your tools environment by running:

eval $(autobuild source_environment)

That only needs to be done once per terminal session.

Build by running:

autobuild build --no-configure  -c configuration

the resulting viewer executable will be at:

build-vc140/newview/configuration/universe-bin.exe


#Building within Visual Studio

Open the VS 2015 solution file

cygstart build-vc140/Universe.sln

Right-click on "universe-bin" in the Solution Explorer and select "Build"

    wait... 

When the build completes (hopefully without errors) run it by pressing Control-F5 

