#Building the viewer on Mac OS X

In order to build the Universe Viewer on Mac OS X please use the following directions.


#Install the Development Tools

In order to build the Universe Viewer, you will need to download a few software packages and install them. These are:

XCode (Version 7 or later): https://developer.apple.com/xcode/
CMake (Version 3.0 or later.  We recommend Version 3.1.0): http://www.cmake.org/
Mercurial (TortoiseHG might work for Mac but we aren't sure): https://www.mercurial-scm.org/downloads
Autobuild (Note: We do have our own autobuild based off the LL code): https://wiki.secondlife.com/wiki/Autobuild
	(Our autobuild can be found at: http://bitbucket.org/secondgalaxydevelopmentteam/autobuild-universe)
	
Please refer to the applicable software distributors for installation instructions.


#Getting the source code

Our source code is hosted in a mercurial repository on Bitbucket and can be cloned to your hard drive with the following command:

hg clone https://bitbucket.org/secondgalaxydevelopmentteam/universe-viewer

Note: We do have a repository on github at: https://github.com/Virtual-Universe/universe-viewer

The Github repository is for development purposes only.  If you wish to build the viewer please use the Bitbucket repository.


#Configuring the Viewer

We highly recommend building with autobuild. While there are ways to build the viewer without autobuild, these methods are both unsupported and unadvised.

For a basic self-compiled build, simply run these commands from the Universe Viewer repository on your local machine:

autobuild configure -c RelWithDebInfoOS -- -DPACKAGE:BOOL=FALSE -DFMODSTUDIO:BOOL=TRUE

For a 64-bit build, add:

-DWORD_SIZE=64


#Building the Viewer

Now that you've successful configured the viewer, start a build with the following command:

autobuild build -c RelWithDebInfoOS --no-configure

If you want to get fancy, you can configure and build in one command, like so:

autobuild build -c RelWithDebInfoOS -- -DPACKAGE:BOOL=FALSE -DFMODSTUDIO:BOOL=TRUE

The viewer may also be built from inside the Xcode IDE once you have configured the viewer, though we do not officially support this method. To do so, simply open Universe.xcodeproj located in the build-i386-darwin directory.


#Running your Newly Built Viewer

You can run your newly created application from the repository's root directory with the following command:

open build-darwin-i386/newview/configuration-type/Universe.app

or, using Finder, navigate to this very same directory and double click Universe Viewer.